<?php
require_once 'classes/api/ApiManager.php';

use Classes\Api\ApiManager;

$api = ApiManager::instance()->getSuitable();
$api->processRequest();
