<?php namespace Classes\Helpers;

class Transformer
{
    public static function xmlToArray($xmlObject, $out = [])
    {
        foreach((array) $xmlObject as $index => $node ) {
            $out[$index] = (is_object($node)) ? self::xmlToArray($node) : $node;
        }

        return $out;
    }

    public static function arrayToXml($array, &$output_xml)
    {
        foreach($array as $key => $value) {
            if(is_array($value)) {
                if(!is_numeric($key)){
                    $subnode = $output_xml->addChild("$key");
                    self::arrayToXml($value, $subnode);
                }else{
                    $subnode = $output_xml->addChild("item$key");
                    self::arrayToXml($value, $subnode);
                }
            } else {
                $output_xml->addChild("$key",htmlspecialchars("$value"));
            }
        }
    }
}
