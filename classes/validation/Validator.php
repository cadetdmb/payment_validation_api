<?php namespace Classes\Validation;

class Validator
{
    const CVV2_ERROR = 1;
    const CVV2_EMPTY = 10;
    const EXP_DATE_ERROR = 2;
    const EXP_DATE_EMPTY = 20;
    const CREDIT_CARD_ERROR = 3;
    const CREDIT_CARD_EMPTY = 30;
    const EMAIL_ERROR = 4;
    const EMAIL_EMPTY = 40;
    const PHONE_NUMBER_ERROR = 5;
    const PHONE_NUMBER_EMPTY = 50;
    const PAYMENT_TYPE_EMPTY = 60;
    const INVALID_HASH = 7;
    const KEY = 'secret_key';

    private $empty_errors = [
        'cvv2' => self::CVV2_EMPTY,
        'exp_date' => self::EXP_DATE_EMPTY,
        'card_number' => self::CREDIT_CARD_EMPTY,
        'email' => self::EMAIL_EMPTY,
        'phone_number' => self::PHONE_NUMBER_EMPTY,
    ];
    private $errors;
    protected $request;

    public function validate($request, $rules, $payment_type)
    {
        $this->request = $request;
        $this->errors = [];
        //check if empty
        foreach($rules as $key => $val) {
            if(empty($request[$val])) {
                $this->errors[] = $this->empty_errors[$val];
            }
        }
        //exit with error_codes, if some empty fields are in request
        if(!empty($this->errors)) {
            throw new \Exception(implode(',', $this->errors));
        }
        //first:ckeck hash, then check fields
        $this->validateHash($request);
        $this->validatePaymentFields($payment_type);

        if(!empty($this->errors)) {
            throw new \Exception(implode(',', $this->errors));
        }
    }

    /**
     * Validates fields depending on payment_type
     * @param  string $payment_type
     * @return void
     */
    protected function validatePaymentFields($payment_type)
    {
        if($payment_type == 'mobile') {
            $this->checkPhoneNumber($this->request['phone_number']);
        } else {
            $this->checkCVV2($this->request['cvv2']);
            $this->checkExpDate($this->request['exp_date']);
            $this->checkCardNumber($this->request['card_number']);
            $this->checkEmail($this->request['email']);
        }
    }

    /**
     * Return false if cvv2 less then 3 or bigger then 4
     * @param  string $value cvv2
     * @return bool
     */
    protected function checkCVV2($value)
    {
        if(is_numeric($value)) {
            if(strlen($value) <= 4 && strlen($value) >= 3) {
                return true;
            }
        }

        $this->errors[] = self::CVV2_ERROR;
        return false;
    }

    /**
     * Checks if exp_date > current date
     * @param  string $value exp_date
     * @return bool
     */
    protected function checkExpDate($value)
    {
        $exp_time = strtotime($value);
        if($exp_time > time()) {
            return true;
        }

        $this->errors[] = self::EXP_DATE_ERROR;
        return false;
    }

    /**
     * Luhn algorithm
     * @param  string $value [card_number]
     * @return bool
     */
    protected function checkCardNumber($value)
    {
        $value = strrev(preg_replace('/[^\d]/', '', $value));
        $sum = 0;
        for ($i = 0, $j = strlen($value); $i < $j; $i++) {
            if (($i % 2) == 0) {
                $val = $value[$i];
            } else {
                $val = $value[$i] * 2;
                if ($val > 9) {
                    $val -= 9;
                }
            }
            $sum += $val;
        }
        if((($sum % 10) == 0)) {
            return true;
        }

        $this->errors[] = self::CREDIT_CARD_ERROR;
        return false;
    }

    /**
     * Native email validation
     * @param  string $value email
     * @return bool
     */
    protected function checkEmail($value)
    {
        if(filter_var($value, FILTER_VALIDATE_EMAIL)) {
            return true;
        }

        $this->errors[] = self::EMAIL_ERROR;
        return false;
    }

    /**
     * Validates phone number, accepts format: 12 digits +_ _ _( _ _)_ _ _ _ _ _ _ or 10 digits: (_ _ _) _ _ _ _ _ _ _
     ** @param  string $value phone number
     * @return bool
     */
    protected function checkPhoneNumber($value)
    {
        $numbers = preg_replace('~\D+~', '', $value);
        $lenght = strlen($numbers);
        if ($lenght == 12 or $lenght == 10) {
            return true;
        }

        $this->errors[] = self::PHONE_NUMBER_ERROR;
        return false;
    }

    /**
     * Validates hash (md5 of all parameters including timestamp + key)
     * @param  array $request
     * @return bool
     */
    protected function validateHash($request)
    {
        if(empty($request['timestamp']) || empty($request['hash'])) {
            $this->errors[] = self::INVALID_HASH;
            return false;
        }
        $income_hash = $request['hash'];
        unset($request['hash']);

        $str = implode('', $request).self::KEY;
        $calculated_hash = md5($str);
        //echo $calculated_hash;
        if($calculated_hash == $income_hash) {
            return true;
        }

        $this->errors[] = self::INVALID_HASH;
        return false;
    }
}
