<?php namespace Classes\Api;
require_once 'classes/helpers/Transformer.php';

use Classes\Helpers\Transformer;

abstract class ApiBase
{
    protected $response;
    protected $request_headers;
    protected $request;

    abstract protected function process(Array $request, \Closure $response);

    public function __construct()
    {
        header("Access-Control-Allow-Orgin: *");
        header("Access-Control-Allow-Methods: *");        
        $this->request_headers = getallheaders();
        $request_type = $this->detectRequestType();
        $this->request = $this->fetchData($request_type);
        $response_type = $this->detectResponseType();

        $this->response = function ($data) use ($response_type) {
            switch ($response_type) {
                case 'xml':
                    return $this->xmlResponse($data);
                break;
                case 'json':
                    return $this->jsonResponse($data);
                break;
            }
        };
    }

    public function processRequest()
    {
        $this->process($this->request, $this->response);
    }

    protected function xmlResponse($data)
    {
        header("Content-Type: application/xml");
        $xml = new \SimpleXMLElement('<root/>');
        Transformer::arrayToXml($data, $xml);
        return $xml->asXML();
    }

    protected function jsonResponse($data)
    {
        header("Content-Type: application/json");
        return json_encode($data);
    }

    protected function detectResponseType()
    {
        if(!empty($this->request_headers['Accept'])) {
            if($this->request_headers['Accept'] == 'application/xml'
            || $this->request_headers['Accept'] == 'text/xml') {
                return 'xml';
            }
        }

        return 'json';
    }

    protected function detectRequestType()
    {
        if(!empty($this->request_headers['Content-Type'])) {
            if($this->request_headers['Content-Type'] == 'application/xml'
            || $this->request_headers['Content-Type'] == 'text/xml') {
                return 'xml';
            }
        }

        return 'json';
    }

    protected function fetchData($type)
    {
        $data = file_get_contents('php://input');
        if($type == 'xml') {
            $reqArr = Transformer::xmlToArray(simplexml_load_string($data));
        } else {
            $reqArr = json_decode(json_encode($data), true);
        }

        return $reqArr;
    }
}
?>
