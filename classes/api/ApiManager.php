<?php namespace Classes\Api;

require_once 'classes/api/PaymentApi.php';

use Classes\Api\PaymentApi;

class ApiManager
{
    protected static $instance;

    /**
     * Function creates suitable Api object
     * @return ApiBase
     */
    public function getSuitable()
    {
        //we have only one api: 'payment'
        return new PaymentApi();
    }

    /**
     * Creates instance of Manager
     */
    public static function instance()
    {
        return isset(static::$instance)
            ? static::$instance
            : static::$instance = new static;
    }
}
