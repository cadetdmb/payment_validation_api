<?php namespace Classes\Api;

require_once 'classes/api/ApiBase.php';
require_once 'classes/validation/Validator.php';

use Classes\Api\ApiBase;
use Classes\Validation\Validator;

class PaymentApi extends ApiBase
{
    protected $rules = [
        'credit_card' => [
            'card_number',
            'exp_date',
            'cvv2'
        ],
        'mobile' => [
            'phone_number'
        ]
    ];
    public function process(Array $request, \Closure $response)
    {
        try {
            if(empty($request['payment_type']) || empty($this->rules[$request['payment_type']])) {
                throw new \Exception(Validator::PAYMENT_TYPE_EMPTY);
            }
            $validator = new Validator();
            $validator->validate($request, $this->rules[$request['payment_type']], $request['payment_type']);
        } catch (\Exception $e) {
            $outcome_data = [
                'valid' => false,
                'error_code' => explode(',', $e->getMessage())
            ];
            echo $response($outcome_data);
            return;
        }
        $outcome_data = [
            'valid' => true
        ];

        echo $response($outcome_data);
    }
}
